--technical stuff
minetest.register_node("time_travel:jungle_tree", {drawtype="airlike",groups = {not_in_creative_inventory=1},})
minetest.register_node("time_travel:snowy_pine_tree", {drawtype="airlike",groups = {not_in_creative_inventory=1},})
minetest.register_node("time_travel:tree", {drawtype="airlike",groups = {not_in_creative_inventory=1},})
--minetest.register_node("time_travel:coral_reef", {drawtype="airlike",groups = {not_in_creative_inventory=1},})

--australia nodes taken from the australia mod

minetest.register_node("time_travel:australia_red_stone", {
	description = "Australian Red Stone",
	tiles = {"time_travel_aus_red_stone.png"},
	groups = {cracky=3, stone=1},
	drop = 'time_travel:australia_red_cobble',
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("time_travel:australia_red_cobble", {
	description = "Australian Red cobblestone",
	tiles = {"time_travel_aus_red_cobble.png"},
	is_ground_content = false,
	groups = {cracky=3, stone=2},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_craft({
    type = "cooking",
    output = "time_travel:australia_red_stone",
    recipe = "time_travel:australia_red_cobble",
    --cooktime = 10,
})

minetest.register_node("time_travel:australia_red_gravel", {
	description = "Australian Red Gravel",
	tiles = {"time_travel_aus_red_gravel.png"},
	groups = {crumbly=2, falling_node=1},
	sounds = default.node_sound_dirt_defaults({
		footstep = {name="default_gravel_footstep", gain=0.5},
		dug = {name="default_gravel_footstep", gain=1.0},
	}),
})

minetest.register_node("time_travel:australia_red_stonebrick", {
	description = "Australian Red Stone Brick",
	tiles = {"time_travel_aus_red_stonebrick.png"},
	is_ground_content = false,
	groups = {cracky=2, stone=1},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_craft({
	output = 'time_travel:australia_red_stonebrick 4',
	recipe = {
		{'time_travel:australia_red_stone', 'time_travel:australia_red_stone'},
		{'time_travel:australia_red_stone', 'time_travel:australia_red_stone'},
	}
})

minetest.register_node("time_travel:australia_red_dirt", {
	description = "Australian Red Dirt",
	tiles = {"time_travel_aus_red_dirt.png"},
	groups = {crumbly=3,soil=1},
	sounds = default.node_sound_dirt_defaults(),
})

minetest.register_node("time_travel:australia_red_sand", {
	description = "Australian Red Sand",
	tiles = {"time_travel_aus_red_sand.png"},
	groups = {crumbly=3, falling_node=1, sand=1},
	sounds = default.node_sound_sand_defaults(),
})

--metamese stuff

minetest.register_craftitem("time_travel:metamese_crystal", {
	description = "Metamese Crystal",
	inventory_image = "time_travel_metamese_crystal.png",
})

minetest.register_craftitem("time_travel:metamese_crystal_fragment", {
	description = "Metamese Crystal Fragment",
	inventory_image = "time_travel_metamese_crystal_fragment.png",
})

minetest.register_node("time_travel:stone_with_metamese", {
	description = "Metamese Ore",
	tiles = {"default_stone.png^time_travel_mineral_metamese.png"},
	paramtype = "light",
	groups = {cracky = 1},
	drop = "time_travel:metamese_crystal",
	sounds = default.node_sound_stone_defaults(),
	light_source = 6,
})

minetest.register_node("time_travel:metamese_block", {
	description = "Metamese Block",
	tiles = {"time_travel_metamese_block.png"},
	paramtype = "light",
	groups = {cracky = 1, level = 3},
	sounds = default.node_sound_stone_defaults(),
	light_source = 15,
})

minetest.register_craft({
	output = 'time_travel:metamese_block',
	recipe = {
		{'time_travel:metamese_crystal', 'time_travel:metamese_crystal', 'time_travel:metamese_crystal'},
		{'time_travel:metamese_crystal', 'time_travel:metamese_crystal', 'time_travel:metamese_crystal'},
		{'time_travel:metamese_crystal', 'time_travel:metamese_crystal', 'time_travel:metamese_crystal'},
	}
})

minetest.register_craft({
	output = 'time_travel:metamese_crystal 9',
	recipe = {
		{'time_travel:metamese_block'},
	}
})

minetest.register_craft({
	output = 'time_travel:metamese_crystal_fragment 9',
	recipe = {
		{'time_travel:metamese_crystal'},
	}
})

minetest.register_craft({
	output = "time_travel:metamese_crystal",
	recipe = {
		{"time_travel:metamese_crystal_fragment", "time_travel:metamese_crystal_fragment", "time_travel:metamese_crystal_fragment"},
		{"time_travel:metamese_crystal_fragment", "time_travel:metamese_crystal_fragment", "time_travel:metamese_crystal_fragment"},
		{"time_travel:metamese_crystal_fragment", "time_travel:metamese_crystal_fragment", "time_travel:metamese_crystal_fragment"},
	}
})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "time_travel:stone_with_metamese",
		wherein        = "default:stone",
		clust_scarcity = 36 * 36 * 36,
		clust_num_ores = 3,
		clust_size     = 2,
		y_min          = -31000,
		y_max          = -1024,
	})

--time travel stuff
minetest.register_craftitem("time_travel:time_core", {
	description = "Time Core",
	inventory_image = "time_travel_time_core.png",
})

minetest.register_craft({
	output = "time_travel:time_core",
	recipe = {
		{"default:gold_ingot", "", "default:gold_ingot"},
		{"", "time_travel:metamese_crystal", ""},
		{"", "default:gold_ingot", ""},
	}
})

minetest.register_node("time_travel:time_machine_inactive", {
	description = "Inactive Time Machine",
	tiles = {
		"time_travel_time_machine_top_inactive.png",
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"default_steel_block.png^time_travel_time_core.png",
	},
	--paramtype = "light",
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {cracky = 1, level = 2},
	sounds = default.node_sound_metal_defaults(),
	--light_source = 15,
	after_place_node = function(pos, placer, itemstack)
			local meta=minetest.get_meta(pos)
			meta:set_string("owner",placer:get_player_name())
			meta:set_string("infotext","Time Machine: Water Age")
	end,
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
		if itemstack:get_name() == "default:junglegrass" and itemstack:get_count() >= 1 then
			local taken = itemstack:take_item(1)
			local inv = player:get_inventory()
			local added = inv:add_item("main", ItemStack({name = "multidimensions:teleporter0", count = 1}))
			local p = minetest.get_node(pos).param2
			minetest.swap_node(pos, {name = "time_travel:time_machine_dinosaur_age",param2=p})
		elseif itemstack:get_name() == "default:snow" and itemstack:get_count() >= 1 then
			local taken = itemstack:take_item(1)
			local inv = player:get_inventory()
			local added = inv:add_item("main", ItemStack({name = "multidimensions:teleporter0", count = 1}))
			local p = minetest.get_node(pos).param2
			minetest.swap_node(pos, {name = "time_travel:time_machine_ice_age",param2=p})
		elseif itemstack:get_name() == "default:dry_shrub" and itemstack:get_count() >= 1 then
			local taken = itemstack:take_item(1)
			local inv = player:get_inventory()
			local added = inv:add_item("main", ItemStack({name = "multidimensions:teleporter0", count = 1}))
			local p = minetest.get_node(pos).param2
			minetest.swap_node(pos, {name = "time_travel:time_machine_prehistoric_australia",param2=p})
		elseif itemstack:get_name() == "default:coral_skeleton" and itemstack:get_count() >= 1 then
			local taken = itemstack:take_item(1)
			local inv = player:get_inventory()
			local added = inv:add_item("main", ItemStack({name = "multidimensions:teleporter0", count = 1}))
			local p = minetest.get_node(pos).param2
			minetest.swap_node(pos, {name = "time_travel:time_machine_water_age",param2=p})
		end
	end
})

minetest.register_craft({
	output = "time_travel:time_machine_inactive",
	recipe = {
		{"default:steelblock", "default:mese_crystal", "default:steelblock"},
		{"default:mese_crystal", "time_travel:time_core", "default:mese_crystal"},
		{"default:steelblock", "default:mese_crystal", "default:steelblock"},
	}
})

minetest.register_node("time_travel:time_machine_dinosaur_age", {
	description = "Time Machine: Dinosaur Age",
	tiles = {
		{
			name = "time_travel_time_machine_top_dinosaur_age_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1.0,
			},
		},
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"default_steel_block.png^time_travel_time_core.png",
	},
	paramtype = "light",
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {cracky = 1, level = 2},
	sounds = default.node_sound_metal_defaults(),
	light_source = 15,
	after_place_node = function(pos, placer, itemstack)
			local meta=minetest.get_meta(pos)
			meta:set_string("owner",placer:get_player_name())
			meta:set_string("infotext","Time Machine: Dinosaur Age")
	end,
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
			local owner=minetest.get_meta(pos):get_string("owner")
			local pos2={x=pos.x,y=2000+501+1+2,z=pos.z}
			if not minetest.is_protected(pos2, owner) then
				multidimensions.move(player,pos2)
			end
	end,
		mesecons = {
			receptor = {state = "off"},
			effector={
				action_on=function(pos, node)
					local owner=minetest.get_meta(pos):get_string("owner")
					local pos2={x=pos.x,y=2000+501+1+2,z=pos.z}
					for i, ob in pairs(minetest.get_objects_inside_radius(pos, 5)) do
						multidimensions.move(ob,pos2)
					end
					return false
				end
			}
		},
	drop = "time_travel:time_machine_inactive"
})

minetest.register_node("time_travel:time_machine_ice_age", {
	description = "Time Machine: Ice Age",
	tiles = {
		{
			name = "time_travel_time_machine_top_ice_age_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1.0,
			},
		},
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"default_steel_block.png^time_travel_time_core.png",
	},
	paramtype = "light",
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {cracky = 1, level = 2},
	sounds = default.node_sound_metal_defaults(),
	light_source = 15,
	after_place_node = function(pos, placer, itemstack)
			local meta=minetest.get_meta(pos)
			meta:set_string("owner",placer:get_player_name())
			meta:set_string("infotext","Time Machine: Ice Age")
	end,
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
			local owner=minetest.get_meta(pos):get_string("owner")
			local pos2={x=pos.x,y=3000+501+1+2,z=pos.z}
			if not minetest.is_protected(pos2, owner) then
				multidimensions.move(player,pos2)
			end
	end,
	mesecons = {
			receptor = {state = "off"},
			effector={
				action_on=function(pos, node)
					local owner=minetest.get_meta(pos):get_string("owner")
					local pos2={x=pos.x,y=3000+501+1+2,z=pos.z}
					for i, ob in pairs(minetest.get_objects_inside_radius(pos, 5)) do
						multidimensions.move(ob,pos2)
					end
					return false
				end
			}
		},
	drop = "time_travel:time_machine_inactive"
})

minetest.register_node("time_travel:time_machine_prehistoric_australia", {
	description = "Time Machine: Prehistoric Australia",
	tiles = {
		{
			name = "time_travel_time_machine_top_prehistoric_australia_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1.0,
			},
		},
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"default_steel_block.png^time_travel_time_core.png",
	},
	paramtype = "light",
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {cracky = 1, level = 2},
	sounds = default.node_sound_metal_defaults(),
	light_source = 15,
	after_place_node = function(pos, placer, itemstack)
			local meta=minetest.get_meta(pos)
			meta:set_string("owner",placer:get_player_name())
			meta:set_string("infotext","Time Machine: Prehistoric Australia")
	end,
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
			local owner=minetest.get_meta(pos):get_string("owner")
			local pos2={x=pos.x,y=4000+501+1+2,z=pos.z}
			if not minetest.is_protected(pos2, owner) then
				multidimensions.move(player,pos2)
			end
	end,
		mesecons = {
			receptor = {state = "off"},
			effector={
				action_on=function(pos, node)
					local owner=minetest.get_meta(pos):get_string("owner")
					local pos2={x=pos.x,y=4000+501+1+2,z=pos.z}
					for i, ob in pairs(minetest.get_objects_inside_radius(pos, 5)) do
						multidimensions.move(ob,pos2)
					end
					return false
				end
			}
		},
	drop = "time_travel:time_machine_inactive"
})

minetest.register_node("time_travel:time_machine_water_age", {
	description = "Time Machine: Water Age",
	tiles = {
		{
			name = "time_travel_time_machine_top_water_age_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1.0,
			},
		},
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"time_travel_time_machine_side.png",
		"default_steel_block.png^time_travel_time_core.png",
	},
	paramtype = "light",
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {cracky = 1, level = 2},
	sounds = default.node_sound_metal_defaults(),
	light_source = 15,
	after_place_node = function(pos, placer, itemstack)
			local meta=minetest.get_meta(pos)
			meta:set_string("owner",placer:get_player_name())
			meta:set_string("infotext","Time Machine: Water Age")
	end,
	on_rightclick = function(pos, node, player, itemstack, pointed_thing)
			local owner=minetest.get_meta(pos):get_string("owner")
			local pos2={x=pos.x,y=5000+511+1+2,z=pos.z}
			if not minetest.is_protected(pos2, owner) then
				multidimensions.move(player,pos2)
			end
	end,
		mesecons = {
			receptor = {state = "off"},
			effector={
				action_on=function(pos, node)
					local owner=minetest.get_meta(pos):get_string("owner")
					local pos2={x=pos.x,y=5000+511+1+2,z=pos.z}
					for i, ob in pairs(minetest.get_objects_inside_radius(pos, 5)) do
						multidimensions.move(ob,pos2)
					end
					return false
				end
			}
		},
	drop = "time_travel:time_machine_inactive"
})
