--Dinosaur Age Animals

dinosaur_age_dimension_start = 2000
dinosaur_age_dimension_end = 2999

mobs:spawn({
	name = "paleotest:brachiosaurus",
	nodes = "default:dirt_with_coniferous_litter", --"default:fern_1",
        neighbours = "group:flora",
	min_light = 14, --1,
	--max_light = 15,
	interval = 60,
	chance = 800, -- 8000 -- 15000
	min_height = dinosaur_age_dimension_start,
	max_height = dinosaur_age_dimension_end,
	day_toggle = true, --nil,
})

mobs:spawn({
	name = "paleotest:elasmosaurus",
	nodes = "default:water_source",
	min_light = 14,
	interval = 60,
	chance = 800, -- 15000
	min_height = dinosaur_age_dimension_start,
	max_height = dinosaur_age_dimension_end,
	day_toggle = true,
})

mobs:spawn({
	name = "paleotest:mosasaurus",
	nodes = "default:water_source",
	min_light = 14,
	interval = 60,
	chance = 800, -- 15000
	min_height = dinosaur_age_dimension_start,
	max_height = dinosaur_age_dimension_end,
	day_toggle = true,
})

mobs:spawn({
	name = "paleotest:pteranodon",
	nodes = "air", --"default:water_source",
	min_light = 14,
	interval = 60,
	chance = 800, -- 15000
	min_height = dinosaur_age_dimension_start,
	max_height = dinosaur_age_dimension_end,
	day_toggle = true,
})

mobs:spawn({
	name = "paleotest:sarcosuchus",
	nodes = "default:water_source",
	min_light = 14,
	interval = 60,
	chance = 800, -- 15000
	min_height = dinosaur_age_dimension_start,
	max_height = dinosaur_age_dimension_end,
	day_toggle = true,
})

mobs:spawn({
	name = "paleotest:spinosaurus",
	nodes = "default:dirt_with_coniferous_litter",
        neighbours = "group:flora",
	min_light = 14,
	interval = 60,
	chance = 800, -- 15000
	min_height = dinosaur_age_dimension_start,
	max_height = dinosaur_age_dimension_end,
	day_toggle = true,
})

mobs:spawn({
	name = "paleotest:stegosaurus",
	nodes = "default:dirt_with_coniferous_litter",
        neighbours = "group:flora",
	min_light = 14,
	interval = 60,
	chance = 800, -- 15000
	min_height = dinosaur_age_dimension_start,
	max_height = dinosaur_age_dimension_end,
	day_toggle = true,
})

mobs:spawn({
	name = "paleotest:triceratops",
	nodes = "default:dirt_with_coniferous_litter",
        neighbours = "group:flora",
	min_light = 14,
	interval = 60,
	chance = 800, -- 15000
	min_height = dinosaur_age_dimension_start,
	max_height = dinosaur_age_dimension_end,
	day_toggle = true,
})

mobs:spawn({
	name = "paleotest:tyrannosaurus",
	nodes = "default:dirt_with_coniferous_litter",
        neighbours = "group:flora",
	min_light = 14,
	interval = 60,
	chance = 800, -- 15000
	min_height = dinosaur_age_dimension_start,
	max_height = dinosaur_age_dimension_end,
	day_toggle = true,
})

mobs:spawn({
	name = "paleotest:velociraptor",
	nodes = "default:dirt_with_coniferous_litter",
        neighbours = "group:flora",
	min_light = 14,
	interval = 60,
	chance = 800, -- 15000
	min_height = dinosaur_age_dimension_start,
	max_height = dinosaur_age_dimension_end,
	day_toggle = true,
})

--Ice Age Animals

ice_age_dimension_start = 3000
ice_age_dimension_end = 3999

mobs:spawn({
	name = "paleotest:dire_wolf",
	nodes = "default:dirt_with_snow",
	min_light = 14,
	interval = 60,
	chance = 800, -- 15000
	min_height = ice_age_dimension_start,
	max_height = ice_age_dimension_end,
	day_toggle = true,
})

mobs:spawn({
	name = "paleotest:elasmotherium",
	nodes = "default:dirt_with_snow",
	min_light = 14,
	interval = 60,
	chance = 800, -- 15000
	min_height = ice_age_dimension_start,
	max_height = ice_age_dimension_end,
	day_toggle = true,
})

mobs:spawn({
	name = "paleotest:mammoth",
	nodes = "default:dirt_with_snow",
	min_light = 14,
	interval = 60,
	chance = 800, -- 15000
	min_height = ice_age_dimension_start,
	max_height = ice_age_dimension_end,
	day_toggle = true,
})

mobs:spawn({
	name = "paleotest:smilodon",
	nodes = "default:dirt_with_snow",
	min_light = 14,
	interval = 60,
	chance = 800, -- 15000
	min_height = ice_age_dimension_start,
	max_height = ice_age_dimension_end,
	day_toggle = true,
})

--Perhistoric Australia Animals

prehistoric_australia_dimension_start = 4000
prehistoric_australia_dimension_end = 4999

mobs:spawn({
	name = "paleotest:procoptodon",
	nodes = "time_travel:australia_red_gravel",
		neighbors = "group:dry_grass",
	min_light = 14,
	interval = 60,
	chance = 100, -- 15000
	min_height = prehistoric_australia_dimension_start,
	max_height = prehistoric_australia_dimension_end,
	day_toggle = true,
})

mobs:spawn({
	name = "paleotest:thylacoleo",
	nodes = "time_travel:australia_red_gravel",
	neighbors = "group:dry_grass",
	min_light = 14,
	interval = 60,
	chance = 100, -- 15000
	min_height = prehistoric_australia_dimension_start,
	max_height = prehistoric_australia_dimension_end,
	day_toggle = true,
})

--Water Age Animals

water_age_dimension_start = 5000
water_age_dimension_end = 5999

mobs:spawn({
	name = "paleotest:dunkleosteus",
	nodes = "default:water_source",
	min_light = 1,
	interval = 1,
	chance = 1, -- 15000
	min_height = water_age_dimension_start,
	max_height = water_age_dimension_end,
	day_toggle = true,
})
